﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContentControllerControl
{
    public class ContentController : ContentControl
    {
        #region DependencyPropetiesEvents
        public delegate void AngleChangedEventHandler(object sender, RoutedPropertyChangedEventArgs<double> e);
        public event AngleChangedEventHandler AngleChanged;
        protected virtual void OnAngleChanged(RoutedPropertyChangedEventArgs<double> e) => AngleChanged?.Invoke(this, e);
        #endregion

        #region DependencyProperties
        private static readonly DependencyProperty RotateCenterProperty =
            DependencyProperty.Register(
                "RotateCenter",
                typeof(Point),
                typeof(ContentController),
                new FrameworkPropertyMetadata(
                    new Point(),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault
                )
            );
        
        public Point RotateCenter
        {
            get => (Point)GetValue(RotateCenterProperty);
            set => SetValue(RotateCenterProperty, value);
        }

        private static readonly DependencyProperty ScaleCenterProperty =
            DependencyProperty.Register(
                "ScaleCenter",
                typeof(Point),
                typeof(ContentController),
                new FrameworkPropertyMetadata(
                    new Point(),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault
                )
            );
        
        public Point ScaleCenter
        {
            get => (Point)GetValue(ScaleCenterProperty);
            set => SetValue(ScaleCenterProperty, value);
        }

        private static readonly DependencyProperty ScaleStepProperty =
            DependencyProperty.Register(
                "ScaleStep",
                typeof(double),
                typeof(ContentController),
                new FrameworkPropertyMetadata(
                    0.1,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault
                )
            );

        public double ScaleStep
        {
            get => (double)GetValue(ScaleStepProperty);
            set => SetValue(ScaleStepProperty, value);
        }

        private static readonly DependencyProperty RotateStepProperty =
            DependencyProperty.Register(
                "RotateStep",
                typeof(double),
                typeof(ContentController),
                new FrameworkPropertyMetadata(
                    3.6,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault
                )
            );

        public double RotateStep
        {
            get => (double)GetValue(RotateStepProperty);
            set => SetValue(RotateStepProperty, value);
        }

        private static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register(
                "Angle",
                typeof(double),
                typeof(ContentController),
                new FrameworkPropertyMetadata(
                    0.0,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    (DependencyObject d, DependencyPropertyChangedEventArgs e) =>
                    {
                        var contentController = d as ContentController;
                        double angleStep = (double)e.NewValue - (double)e.OldValue;
                        contentController.RotateAtPoint(angleStep, contentController.RotateCenter);
                    },
                    (DependencyObject d, object baseValue) =>
                    {
                        var angle = (double)baseValue % 360;
                        return (angle < 0) ? angle + 360 : angle;
                    }
                )
            );

        public double Angle
        {
            get => (double)GetValue(AngleProperty);
            set => SetValue(AngleProperty, value);
        }

        private static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register(
                "Scale",
                typeof(double),
                typeof(ContentController),
                new FrameworkPropertyMetadata(
                    1.0,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    (DependencyObject d, DependencyPropertyChangedEventArgs e) =>
                    {
                        var contentController = d as ContentController;
                        double scaleStep = (double)e.NewValue - (double)e.OldValue;
                        contentController.ScaleAtPoint(scaleStep, contentController.ScaleCenter);
                    }
                )
            );

        public double Scale
        {
            get => (double)GetValue(ScaleProperty);
            set => SetValue(ScaleProperty, value);
        }

        private static readonly DependencyProperty TranslateProperty =
            DependencyProperty.Register(
                "Translate",
                typeof(Point),
                typeof(ContentController),
                new FrameworkPropertyMetadata(
                    new Point(),
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                    (DependencyObject d, DependencyPropertyChangedEventArgs e) =>
                    {
                        var contentController = d as ContentController;
                        Point newValue = (Point)e.NewValue;
                        Point oldValue = (Point)e.OldValue;
                        Point translateStep = new Point(newValue.X - oldValue.X, newValue.Y - oldValue.Y);
                        contentController.TranslateByPoint(translateStep);
                    }
                )
            );

        public Point Translate
        {
            get => (Point)GetValue(TranslateProperty);
            set => SetValue(TranslateProperty, value);
        }
        #endregion

        #region Constructors
        static ContentController()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ContentController), 
                new FrameworkPropertyMetadata(typeof(ContentController)));
        }
        #endregion

        #region EventHandlers
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (Content == null)
                return;

            FrameworkElement content = Content as FrameworkElement;
            Rect contentRect = content.TransformToAncestor(this)
                .TransformBounds(new Rect(0, 0, content.ActualWidth, content.ActualHeight));

            Mouse.Capture(content);

            Vector currentPosition = (Vector)Mouse.GetPosition(this);
            Vector centerPoint = new Vector(contentRect.Width / 2, contentRect.Height / 2) + (Vector)contentRect.TopLeft;
            Vector currentVector = currentPosition - centerPoint;

            holdMouseVector = currentVector;
            holdAngleValue = Angle;

            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            Mouse.Capture(null);
            base.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            MouseTranslate();
            base.OnMouseMove(e);
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (Content == null)
                return;

            FrameworkElement content = Content as FrameworkElement;
            Point point = Mouse.GetPosition(content);
            //Point point = new Point(ScaleCenterX, ScaleCenterY);

            if (Mouse.RightButton == MouseButtonState.Pressed)
            {
                Point oldRotateCenter = RotateCenter;
                RotateCenter = point;
                if (e.Delta > 0)
                {
                    Angle += RotateStep;
                }
                else
                {
                    Angle -= RotateStep;
                }
                RotateCenter = oldRotateCenter;
            }
            else
            {
                if (e.Delta > 0)
                    ScaleAtPoint(ScaleStep, point);
                else
                    ScaleAtPoint(-ScaleStep, point);
            }

            base.OnMouseWheel(e);
        }
        #endregion

        #region PublicMethods
        public void RotateAtPoint(double angleStep, Point point)
        {
            SetTransformations(angleStep: angleStep, rotateCenter: point);
        }

        public void ScaleAtPoint(double scaleValue, Point point)
        {
            SetTransformations(scaleStep: scaleValue, scaleCenter: point);
        }

        public void TranslateByPoint(Point point)
        {
            SetTransformations(translateStep: point);
        }

        private void MouseTranslate()
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                Point mouseLocation = Mouse.GetPosition(null);
                Point translateDifference = (Point)Point.Subtract(mouseLocation, oldMouseLocation);
                oldMouseLocation = mouseLocation;

                SetTransformations(translateStep: translateDifference);
            }
            else
            {
                oldMouseLocation = Mouse.GetPosition(null);
            }
        }

        public void ResetTransformations()
        {
            SetCurrentValue(AngleProperty, AngleProperty.GetMetadata(this).DefaultValue);
            SetCurrentValue(ScaleProperty, ScaleProperty.GetMetadata(this).DefaultValue);
            SetCurrentValue(TranslateProperty, TranslateProperty.GetMetadata(this).DefaultValue);
            FrameworkElement content = Content as FrameworkElement;
            content.RenderTransform = new MatrixTransform(Matrix.Identity);
        }
        #endregion

        #region PrivateMethods
        private void SetTransformations(double scaleStep = 0, double angleStep = 0, Point translateStep = new Point(),
            Point scaleCenter = new Point(), Point rotateCenter = new Point())
        {
            if (Content == null)
                return;

            FrameworkElement content = Content as FrameworkElement;
            Matrix matrix = content.RenderTransform.Value;
            double scaleRatio = 1 + scaleStep;

            if (new Point() == scaleCenter)
                matrix.ScalePrepend(scaleRatio, scaleRatio);
            else
                matrix.ScaleAtPrepend(scaleRatio, scaleRatio, scaleCenter.X, scaleCenter.Y);

            if (new Point() == rotateCenter)
                matrix.RotatePrepend(angleStep);
            else
                matrix.RotateAtPrepend(angleStep, rotateCenter.X, rotateCenter.Y);

            if (translateStep.X != 0)
                matrix.OffsetX += translateStep.X;

            if (translateStep.Y != 0)
                matrix.OffsetY += translateStep.Y;

            content.RenderTransform = new MatrixTransform(matrix);
        }
        #endregion

        #region PrivateFields
        private double holdAngleValue;
        private Vector holdMouseVector;
        private Point oldMouseLocation;
        #endregion
    }
}