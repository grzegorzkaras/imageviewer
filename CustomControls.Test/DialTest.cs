﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DialControl;

[assembly: Apartment(System.Threading.ApartmentState.STA)]
namespace CustomControls.Test
{
    [TestFixture]
    public class DialTest
    {
        [Test]
        public void AngleInRange()
        {
            Dial control = new Dial();

            double[] angles = {
                 180,  359,  360,  361,  720,  721,
                -180, -359, -360, -361, -720, -721
            };

            double[] expectedResults =
            {
                180, 359, 0, 1, 0, 1,
                180, 1, 0, 359, 0, 359
            };
            
            Warn.If(angles.Length != expectedResults.Length, 
                "Angles array length is not same as Results array length, additional tests " +
                "won't be made or IndexOutOfRangeException will occur.");

            for(int i = 0; i < expectedResults.Length; i++)
            {
                control.Angle = angles[i];
                Assert.AreEqual(expectedResults[i], control.Angle);
            }
        }

    }
}
