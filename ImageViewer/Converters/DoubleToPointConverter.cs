﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ImageViewer.Converters
{
    class DoubleToPointConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == null || values[0] == DependencyProperty.UnsetValue ||
                values[1] == null || values[1] == DependencyProperty.UnsetValue)
                return DependencyProperty.UnsetValue;

            try
            {
                double x = System.Convert.ToDouble(values[0]);
                double y = System.Convert.ToDouble(values[1]);
                return new Point(x, y);
            }
            catch (Exception)
            {
                return DependencyProperty.UnsetValue;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            object[] unsetValueArray = new object[] { DependencyProperty.UnsetValue, DependencyProperty.UnsetValue };

            if (value == null || value == DependencyProperty.UnsetValue)
                return unsetValueArray;

            try
            {
                TypeConverter converter = TypeDescriptor.GetConverter(value);
                if (converter.CanConvertTo(typeof(Point)))
                {
                    Point point = (Point)converter.ConvertTo(value, typeof(Point));
                    return new object[] { point.X, point.Y };
                }
                else
                {
                    return unsetValueArray;
                }
            }
            catch (Exception)
            {
                return unsetValueArray;
            }
        }
    }
}
