﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ImageViewer.Views;
using System.IO;

namespace ImageViewer
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            string name = "UnhandledExceptionLog.txt";
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string currentDate = DateTime.Now.ToLocalTime().ToString();
            string errorMessage = Environment.NewLine + currentDate + ":" + Environment.NewLine + e.ExceptionObject.ToString() + Environment.NewLine;
            File.AppendAllText(path + name, errorMessage);
        }
    }
}
