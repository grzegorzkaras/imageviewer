﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer.Services
{
    public class Rotator<T>
    {
        private T[] array = default(T[]);
        private int index = 0;

        public int Index { get; set; }

        public Rotator(T[] rotatorArray)
        {
            array = rotatorArray;
        }

        public Rotator(T[] rotatorArray, int startIndex)
        {
            array = rotatorArray;
            index = startIndex;
        }

        public void SetIndex(int startIndex)
        {
            index = startIndex;
        }

        public int IndexOf(T element)
        {
            return Array.IndexOf(array, element);
        }

        public T Next()
        {
            index = (++index < array.Length) ? index : 0;
            return array[index];
        }

        public T Previous()
        {
            index = (index-- > 0) ? index : array.Length - 1;
            return array[index];
        }

        public T Current()
        {
            return array[index];
        }
    }
}
