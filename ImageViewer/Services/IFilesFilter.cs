﻿namespace ImageViewer.Services
{
    public interface IFilesFilter
    {
        string[] FilterAll(string[] paths);
    }
}