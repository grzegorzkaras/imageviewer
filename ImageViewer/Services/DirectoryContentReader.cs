﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;

namespace ImageViewer.Services
{
    public class DirectoryContentReader
    {
        public IFilesFilter FilesFilter { get; private set; }

        public int FilesCount { get; private set; }
        public string[] FilesPaths { get; private set; }

        public DirectoryContentReader(IFilesFilter filesFilter)
        {
            FilesFilter = filesFilter;
            FilesCount = 0;
        }

        public string[] GetFiles(string directoryPath)
        {
            if (FilesFilter == null)
                throw new NullReferenceException("FilesFilter is not set.");

            string[] filesPaths = GetFilesPathsFromExplorer(directoryPath).ToArray();
            return GetFiles(filesPaths);
        }

        public string[] GetFiles(string[] filesPaths)
        {
            if (FilesFilter == null)
                throw new NullReferenceException("FilesFilter is not set.");

            string[] imagesPaths = FilesFilter.FilterAll(filesPaths);
            FilesCount = filesPaths.Length;
            FilesPaths = filesPaths;
            return imagesPaths;
        }

        // References: Microsoft Internet Controls && Microsoft Shell Controls And Automation
        private List<string> GetFilesPathsFromExplorer(string directoryPath)
        {
            /*  Poprawić wyświetlanie gifów, pewnie chodzi o niezdefiniowaną zmienną z getquiery
             *  Naprawić pozyskiwanie plików z pulpitu i lokalizacji wirtualnych (np: wyniki wyszukiwania w explorerze)
             *  https://msdn.microsoft.com/en-us/library/windows/desktop/gg314982(v=vs.85).aspx
             *  https://www.codeproject.com/Articles/15059/C-File-Browser#TheShellclasses
             *  
             *  https://msdn.microsoft.com/en-us/library/windows/desktop/aa752084(v=vs.85).aspx
             *  https://stackoverflow.com/questions/8826389/get-current-names-of-windows-special-folders
             *  https://stackoverflow.com/questions/8810606/identify-all-open-system-special-folders
             *  https://msdn.microsoft.com/en-us/library/windows/desktop/bb774086(v=vs.85).aspx
             */
            if (directoryPath == Environment.GetFolderPath(Environment.SpecialFolder.Desktop))
                return GetFilesPathsFromDesktop();

            string filename;
            List<string> explorerItems = new List<string>();

            SHDocVw.ShellWindows shellWindows = new SHDocVw.ShellWindows();
            for (int i = 0; i < shellWindows.Count; i++)
            {
                SHDocVw.InternetExplorer window = shellWindows.Item(i);

                if (window.LocationURL == String.Empty)
                    continue;

                string windowURL = new Uri(window.LocationURL).LocalPath;
                if (windowURL == directoryPath)
                {
                    filename = Path.GetFileNameWithoutExtension(window.FullName).ToLower();
                    if (filename.ToLowerInvariant() == "explorer")
                    {
                        Shell32.Folder folder = ((Shell32.IShellFolderViewDual2)window.Document).Folder;
                        Shell32.FolderItems items = folder.Items();
                        foreach (Shell32.FolderItem item in items)
                        {
                            if (!item.IsFolder && !item.IsLink)
                                explorerItems.Add(item.Path);
                        }
                    }
                }
            }
            return explorerItems;
        }

        private List<string> GetFilesPathsFromDesktop()
        {
            List<string> explorerItems = new List<string>();
            Shell32.Shell shell = new Shell32.Shell();
            Shell32.Folder desktopFolder = shell.NameSpace(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
            Shell32.FolderItems desktopItems = desktopFolder.Items();
            foreach (Shell32.FolderItem item in desktopItems)
            {
                if (!item.IsFolder && !item.IsLink)
                    explorerItems.Add(item.Path);
            }
            return explorerItems;
        }
    }

}
