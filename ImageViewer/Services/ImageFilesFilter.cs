﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

//https://stackoverflow.com/questions/23520077/wpf-image-collison-detection
namespace ImageViewer.Services
{
    public class ImageFilesFilter : IFilesFilter
    {
        public List<string> SupportedExtensions { get; set; } = 
            new List<string>(new string[] {".bmp", ".jpeg", ".jpg", ".png", ".tiff", ".gif", ".ico"});
        private const string WICDecoderCategory = "{7ED96837-96F0-4812-B211-F13C24117ED3}";
        private RegistryKey baseKey;

        public ImageFilesFilter()
        {
            string baseKeyPath;

            if (Environment.Is64BitOperatingSystem && !Environment.Is64BitProcess)
                baseKeyPath = "Wow6432Node\\CLSID";
            else
                baseKeyPath = "CLSID";

            baseKey = Registry.ClassesRoot.OpenSubKey(baseKeyPath, false);
            RecalculateExtensions();
        }

        private void RecalculateExtensions()
        {
            List<string> extensions = GetSupportedExtensions();
            SupportedExtensions.AddRange(extensions);
        }

        private List<string> GetSupportedExtensions()
        {
            var decoders = GetAdditionalDecoders();
            List<string> rtnlist = new List<string>();

            foreach (var decoder in decoders)
            {
                string[] extensions = decoder.FileExtensions.Split(',');
                foreach (var extension in extensions) rtnlist.Add(extension);
            }
            return rtnlist;
        }

        private IEnumerable<DecoderInfo> GetAdditionalDecoders()
        {
            var result = new List<DecoderInfo>();

            foreach (var codecKey in GetCodecKeys())
            {
                DecoderInfo decoderInfo = new DecoderInfo
                {
                    FriendlyName = Convert.ToString(codecKey.GetValue("FriendlyName", "")),
                    FileExtensions = Convert.ToString(codecKey.GetValue("FileExtensions", ""))
                };
                result.Add(decoderInfo);
            }
            return result;
        }

        private struct DecoderInfo
        {
            public string FriendlyName;
            public string FileExtensions;
        }

        private IEnumerable<RegistryKey> GetCodecKeys()
        {
            var result = new List<RegistryKey>();

            if (baseKey != null)
            {
                var categoryKey = baseKey.OpenSubKey(WICDecoderCategory + "\\instance", false);

                if (categoryKey != null)
                {
                    var codecGuids = categoryKey.GetSubKeyNames();

                    foreach (var codecGuid in GetCodecGuids())
                    {
                        var codecKey = baseKey.OpenSubKey(codecGuid);
                        if (codecKey != null)
                        {
                            result.Add(codecKey);
                        }
                    }
                }
            }

            return result;
        }

        private string[] GetCodecGuids()
        {
            if (baseKey != null)
            {
                var categoryKey = baseKey.OpenSubKey(WICDecoderCategory + "\\instance", false);
                if (categoryKey != null)
                {
                    return categoryKey.GetSubKeyNames();
                }
            }
            return null;
        }

        public string[] FilterAll(string[] paths)
        {
            List<string> correctImageFilesPaths = new List<string>(paths.Length);

            for (int i = 0; i < paths.Length; i++)
            {
                if (SupportedExtensions.Exists((el) => { return paths[i].ToLower().EndsWith(el); }))
                {
                    correctImageFilesPaths.Add(paths[i]);
                }
            }
            
            return correctImageFilesPaths.ToArray();
        }

    }
}