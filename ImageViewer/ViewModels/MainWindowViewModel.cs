﻿using ImageViewer.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Runtime.InteropServices;
using System.Windows.Input;
using System.Net;

//https://stackoverflow.com/questions/17736160/good-way-to-check-if-file-extension-is-of-an-image-or-not
namespace ImageViewer.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Properties
        private string ImagePath { get; set; }
        private string DirectoryPath { get; set; }
        private Rotator<string> PathRotator { get; set; }
        private DirectoryContentReader DirectoryReader { get; set; } = new DirectoryContentReader(new ImageFilesFilter());
        #endregion

        #region Commands
        public RelayCommand<DragEventArgs> DragAndDropCommand { get; private set; }
        public RelayCommand PreviousImageChangeCommand { get; private set; }
        public RelayCommand NextImageChangeCommand { get; private set; }
        public RelayCommand OpenFullscreenCommand { get; private set; }
        public RelayCommand CloseFullscreenCommand { get; private set; }
        #endregion

        #region BindableProperties
        private BitmapImage imageSource;
        public BitmapImage ImageSource
        {
            get => imageSource;
            set => Set<BitmapImage>(ref imageSource, value);
        }

        private bool isImageGif;
        public bool IsImageGif
        {
            get => isImageGif;
            set => Set<bool>(ref isImageGif, value);
        }

        private string textInformation = "Drag and drop image here!";
        public string TextInformation
        {
            get => textInformation;
            set => Set<string>(ref textInformation, value);
        }

        private bool isFullscreenOn = false;
        public bool IsFullscreenOn
        {
            get => isFullscreenOn;
            set => Set<bool>(ref isFullscreenOn, value);
        }
        #endregion

        #region Constructors
        public MainWindowViewModel()
        {
            DragAndDropCommand = new RelayCommand<DragEventArgs>(OnDragAndDrop);
            PreviousImageChangeCommand = new RelayCommand(OnPreviousImageChange);
            NextImageChangeCommand = new RelayCommand(OnNextImageChange);
            OpenFullscreenCommand = new RelayCommand(OnOpenFullscreen);
            CloseFullscreenCommand = new RelayCommand(OnCloseFullscreen);

            OnOpenWithApp();
        }
        #endregion

        #region EventHandlers
        private void OnCloseFullscreen() => IsFullscreenOn = false;

        private void OnOpenFullscreen() => IsFullscreenOn = true;

        public void OnDragAndDrop(DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                string imagePath = files[0];
                try
                {
                    SetFileImage(imagePath);
                    SetDirectory(imagePath);
                    SetCurrentImageRotatorIndex();
                }
                catch (Exception ex) when (ex is NotSupportedException || ex is COMException || ex is FileFormatException)
                {
                    RemoveCurrentImage();
                    TextInformation = $"Could not open: \n{imagePath}\n" +
                            $"The file is not supported.";
                }
                catch (DirectoryNotFoundException)
                {
                    RemoveCurrentImage();
                    TextInformation = $"Could not find directory: \n{Path.GetDirectoryName(imagePath)}\n" +
                        $"Verify the directory location and try again.";
                }
            }
            /*
            else if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                string imagePath = (string)e.Data.GetData(DataFormats.StringFormat);
                try
                {
                    SetURLImage(imagePath);
                }
                catch (Exception ex) when (ex is NotSupportedException || ex is COMException || ex is FileFormatException)
                {
                    RemoveCurrentImage();
                    TextInformation = $"Could not open: \n{imagePath}\n" +
                            $"The file is not supported.";
                }
            }
            */
        }

        public void OnPreviousImageChange()
        {
            if (PathRotator == null)
                return;

            string imagePath = PathRotator.Previous();

            try
            {
                SetFileImage(imagePath);
            }
            catch (Exception)
            {
                RemoveCurrentImage();
                TextInformation = $"Could not open image: \n{imagePath} located in: \n{DirectoryPath}";
            }
        }

        public void OnNextImageChange()
        {
            if (PathRotator == null)
                return;

            string imagePath = PathRotator.Next();

            try
            {
                SetFileImage(imagePath);
            }
            catch (Exception)
            {
                RemoveCurrentImage();
                TextInformation = $"Could not open image: \n{imagePath} located in: \n{DirectoryPath}";
            }
        }
        #endregion

        #region SetImageMethods
        /*
        public void SetURLImage(string imagePath)
        {
            IsImageGif = false;
            TextInformation = String.Empty;
            ImagePath = imagePath;
            ImageSource = InitializeURLImage(imagePath);
            IsImageGif = (imagePath.EndsWith(".gif")) ? true : false;
        }
        */

        public void SetFileImage(string imagePath)
        {
            if (!ImageExist(imagePath))
                return;

            IsImageGif = false;
            TextInformation = String.Empty;
            ImagePath = imagePath;
            ImageSource = InitalizeFileImage(imagePath);
            IsImageGif = (imagePath.EndsWith(".gif")) ? true : false;
        }

        private bool ImageExist(string imagePath)
        {
            if (!File.Exists(imagePath))
            {
                RemoveCurrentImage();
                TextInformation = $"Could not find image: \n{imagePath}\n" +
                    $"Verify the image location and try again.";
                return false;
            }
            return true;
        }

        private BitmapImage InitalizeFileImage(string imagePath)
        {
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(imagePath);
            image.EndInit();
            return image;
        }
        /*
        private BitmapImage InitializeURLImage(string imagePath)
        {
            WebRequest request = WebRequest.CreateDefault(new Uri(imagePath));
            byte[] buffer = new byte[4096];

            using (FileStream target = new FileStream("URLImage", FileMode.Create, FileAccess.ReadWrite))
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        int read;

                        while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                            target.Write(buffer, 0, read);

                        BitmapImage image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.StreamSource = target;
                        image.EndInit();
                        return image;
                    }
                }
            }
        }
        */

        private void RemoveCurrentImage()
        {
            IsImageGif = false;
            ImagePath = String.Empty;
            ImageSource = null;
        }
        #endregion

        #region SetDirectoryMethods
        private void SetDirectory(string path)
        {
            string directoryPath = Path.GetDirectoryName(path);
            if (!DirectoryExist(directoryPath))
                throw new DirectoryNotFoundException();

            if (DirectoryPath == directoryPath)
                return;
            
            DirectoryPath = directoryPath;
            SetRotator(directoryPath);
        }

        private bool DirectoryExist(string directoryPath)
        {
            if(!Directory.Exists(directoryPath))
            {
                RemoveCurrentImage();
                TextInformation = $"Could not find directory: \n{directoryPath}\n" +
                    $"Verify the directory location and try again.";
                return false;
            }
            return true;
        }
        #endregion

        #region RotatorMethods
        private void SetRotator(string directoryPath)
        {
            string[] imageFilesPaths = DirectoryReader.GetFiles(directoryPath);
            if (imageFilesPaths.Length > 0)
                PathRotator = new Rotator<string>(imageFilesPaths);
        }

        private void SetCurrentImageRotatorIndex()
        {
            if (PathRotator == null)
                return;

            int indexOfImage = PathRotator.IndexOf(ImagePath);
            if (indexOfImage != -1)
                PathRotator.SetIndex(indexOfImage);
        }
        #endregion

        #region OpenWithApp
        private void OnOpenWithApp()
        {
            string[] arguments = Environment.GetCommandLineArgs();

            if (arguments.Length == 2)
            {
                string imagePath = arguments[1];
                try
                {
                    SetFileImage(imagePath);
                    SetDirectory(imagePath);
                    SetCurrentImageRotatorIndex();
                }
                catch (Exception ex) when (ex is NotSupportedException || ex is COMException || ex is FileFormatException)
                {
                    RemoveCurrentImage();
                    TextInformation = $"Could not open image: \n{imagePath}\n" +
                        $"Verify that you have opened the correct file.";
                }
                catch (DirectoryNotFoundException)
                {
                    RemoveCurrentImage();
                    TextInformation = $"Could not find directory: \n{Path.GetDirectoryName(imagePath)}\n" +
                        $"Verify the directory location and try again.";
                }
            }
        }
        #endregion
    }
}
