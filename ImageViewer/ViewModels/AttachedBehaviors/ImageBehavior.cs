﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Media;

// Visibility is important
// https://stackoverflow.com/questions/210922/how-do-i-get-an-animated-gif-to-work-in-wpf
// https://martinfowler.com/articles/injection.html
// https://github.com/XamlAnimatedGif/WpfAnimatedGif/blob/master/WpfAnimatedGif/ImageBehavior.cs

namespace ImageViewer.AttachedBehaviors
{
    class ImageBehavior : DependencyObject
    {
        private static double imageDpiX;
        private static double imageDpiY;
        private static int imageWidth;
        private static int imageHeight;

        private static readonly string delayQuery = "/grctlext/Delay";
        private static readonly string widthQuery = "/imgdesc/Width";
        private static readonly string heightQuery = "/imgdesc/Height";
        private static readonly string leftQuery = "/imgdesc/Left";
        private static readonly string topQuery = "/imgdesc/Top";
        private static readonly string disposalQuery = "/grctlext/Disposal";

        private static AnimationTimeline Animation { get; set; } = null;
        public static int DefaultFrameSpeed { get; set; } = 100;

        private static DependencyObject CurrentDependencyObject { get; set; }

        public static readonly DependencyProperty AnimationDurationProperty =
            DependencyProperty.RegisterAttached(
                "AnimationDuration",
                typeof(int),
                typeof(ImageBehavior),
                new PropertyMetadata(0)
            );

        public static int GetAnimationDuration(DependencyObject d)
        {
            return (int)d.GetValue(AnimationDurationProperty);
        }

        public static void SetAnimationDuration(DependencyObject d, int value)
        {
            d.SetValue(AnimationDurationProperty, value);
        }

        public static readonly DependencyProperty AnimationProgressTimeProperty =
            DependencyProperty.RegisterAttached(
                "AnimationProgressTime",
                typeof(int),
                typeof(ImageBehavior),
                new PropertyMetadata(0)
            );

        public static int GetAnimationProgressTime(DependencyObject d)
        {
            return (int)d.GetValue(AnimationProgressTimeProperty);
        }

        public static void SetAnimationProgressTime(DependencyObject d, int value)
        {
            d.SetValue(AnimationProgressTimeProperty, value);
        }

        public static readonly DependencyProperty AnimateGifProperty =
            DependencyProperty.RegisterAttached(
                "AnimateGif",
                typeof(bool), 
                typeof(ImageBehavior), 
                new PropertyMetadata(
                    false, 
                    new PropertyChangedCallback(AnimateGifChanged)
                )
            );

        public static bool GetAnimateGif(DependencyObject d)
        {
            return (bool)d.GetValue(AnimateGifProperty);
        }

        public static void SetAnimateGif(DependencyObject d, bool value)
        {
            d.SetValue(AnimateGifProperty, value);
        }

        private static void AnimateGifChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d == null)
                return;

            Image image = d as Image;
            CurrentDependencyObject = d;

            if ((bool)e.NewValue == true)
            {
                BitmapImage imageSource = image.Source as BitmapImage;
                if (imageSource == null)
                    return;

                imageDpiX = imageSource.DpiX;
                imageDpiY = imageSource.DpiY;
                imageWidth = imageSource.PixelWidth;
                imageHeight = imageSource.PixelHeight;

                Animation = InitializeAnimation(imageSource);
                if (Animation == null)
                    return;

                StartAnimation(image, Animation);
            }
            else
            {
                if (Animation == null)
                    return;

                StopAnimation(image);
                Animation.CurrentTimeInvalidated -= Animation_CurrentTimeInvalidated;
                SetAnimationProgressTime(CurrentDependencyObject, 0);
                SetAnimationDuration(CurrentDependencyObject, 0);
                Animation = null;
            }
        }

        private static AnimationTimeline InitializeAnimation(BitmapImage imageSource)
        {
            GifBitmapDecoder gifDecoder = new GifBitmapDecoder(imageSource.UriSource,
                BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

            if (gifDecoder.Frames.Count < 1)
                return null;

            BitmapFrame oldFrame = null;
            int animationDuration = 0;

            BitmapFrame firstFrame = gifDecoder.Frames[0];
            byte firstFrameDisposalMethod = (byte)((BitmapMetadata)firstFrame.Metadata).GetQuery(disposalQuery);
            ObjectAnimationUsingKeyFrames animation = new ObjectAnimationUsingKeyFrames();

            int framesCount = gifDecoder.Frames.Count;
            for (int i = 0; i < framesCount; i++)
            {
                BitmapFrame frame = gifDecoder.Frames[i];
                frame.Freeze();
                KeyTime keyTime = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(animationDuration));
                BitmapMetadata frameMetadata = frame.Metadata as BitmapMetadata;
                int frameDelay = (ushort)frameMetadata.GetQuery(delayQuery) * 10;
                byte disposalMethod = (byte)frameMetadata.GetQuery(disposalQuery);

                if (i != 0 && disposalMethod.Equals(1))
                    frame = CombineFrames(oldFrame, frame);
                
                if (i == framesCount - 1 && firstFrameDisposalMethod.Equals(1))
                    frame = CombineFrames(firstFrame, frame);

                animation.KeyFrames.Add(new DiscreteObjectKeyFrame(frame, keyTime));
                oldFrame = frame;
                animationDuration += (frameDelay == 0) ? DefaultFrameSpeed : frameDelay;
            }
            animation.RepeatBehavior = RepeatBehavior.Forever;
            animation.CurrentTimeInvalidated += Animation_CurrentTimeInvalidated;
            animation.Duration = new Duration(TimeSpan.FromMilliseconds(animationDuration));

            SetAnimationDuration(CurrentDependencyObject, animationDuration);

            return animation;
        }

        private static void Animation_CurrentTimeInvalidated(object sender, EventArgs e)
        {
            AnimationClock animationClock = (AnimationClock)sender;
            TimeSpan? currentTime = animationClock.CurrentTime;
            if (currentTime != null)
                SetAnimationProgressTime(CurrentDependencyObject, (int)currentTime.Value.TotalMilliseconds);
        }

        private static BitmapFrame CombineFrames(BitmapFrame firstFrame, BitmapFrame secondFrame)
        {
            int firstFrameLeftOffset = 0;
            int firstFrameTopOffset = 0;
            int firstFrameWidth = imageWidth;
            int firstFrameHeight = imageHeight;

            int secondFrameLeftOffset = 0;
            int secondFrameTopOffset = 0;
            int secondFrameWidth = imageWidth;
            int secondFrameHeight = imageHeight;
            
            if (firstFrame.Metadata is BitmapMetadata firstFrameMetadata)
            {
                firstFrameLeftOffset = (ushort)firstFrameMetadata.GetQuery(leftQuery);
                firstFrameTopOffset = (ushort)firstFrameMetadata.GetQuery(topQuery);
                firstFrameWidth = (ushort)firstFrameMetadata.GetQuery(widthQuery);
                firstFrameHeight = (ushort)firstFrameMetadata.GetQuery(heightQuery);
            }

            if (secondFrame.Metadata is BitmapMetadata secondFrameMetadata)
            {
                secondFrameLeftOffset = (ushort)secondFrameMetadata.GetQuery(leftQuery);
                secondFrameTopOffset = (ushort)secondFrameMetadata.GetQuery(topQuery);
                secondFrameWidth = (ushort)secondFrameMetadata.GetQuery(widthQuery);
                secondFrameHeight = (ushort)secondFrameMetadata.GetQuery(heightQuery);
            }

            DrawingVisual drawingVisual = new DrawingVisual();
            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawImage(firstFrame, new Rect(
                    firstFrameLeftOffset, firstFrameTopOffset, firstFrameWidth, firstFrameHeight));
                drawingContext.DrawImage(secondFrame, new Rect(
                    secondFrameLeftOffset, secondFrameTopOffset, secondFrameWidth, secondFrameHeight));
            }

            RenderTargetBitmap bmp = new RenderTargetBitmap(
                imageWidth, imageHeight, imageDpiX, imageDpiY, PixelFormats.Pbgra32);
            bmp.Render(drawingVisual);

            return BitmapFrame.Create(bmp);
        }

        private static void StartAnimation(Image image, AnimationTimeline animation)
        {
            image.BeginAnimation(Image.SourceProperty, animation, HandoffBehavior.SnapshotAndReplace);
        }

        private static void StopAnimation(Image image)
        {
            if (DependencyPropertyHelper.GetValueSource(image, Image.SourceProperty).IsAnimated)
            {
                image.BeginAnimation(Image.SourceProperty, null);
            }
        }
    }
}