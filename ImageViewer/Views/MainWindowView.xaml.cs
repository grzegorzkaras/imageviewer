﻿using ImageViewer.ViewModels;
using ImageViewer.Views.Adorners;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ImageViewer.Views
{
    public partial class MainWindowView : Window
    {
        private Cursor grabCursor;
        private Cursor grabbingCursor;

        private DragAdorner dragAdorner = null;
        private bool isDragAdornerAdded = false;

        public MainWindowView()
        {
            InitializeComponent();
            InitializeCursors();
            SetDefaultSettings();
            dragAdorner = new DragAdorner(ImageController);
        }

        #region SystemCommands
        private void ShowSystemMenu_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;
        private void ShowSystemMenu_Executed(object sender, ExecutedRoutedEventArgs e) => SystemCommands.ShowSystemMenu(this, PointToScreen(new Point(-165, 35)));

        private void MinimizeWindow_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;
        private void MinimizeWindow_Executed(object sender, ExecutedRoutedEventArgs e) => SystemCommands.MinimizeWindow(this);

        private void MaximizeWindow_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;
        private void MaximizeWindow_Executed(object sender, ExecutedRoutedEventArgs e) => SystemCommands.MaximizeWindow(this);

        private void RestoreWindow_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;
        private void RestoreWindow_Executed(object sender, ExecutedRoutedEventArgs e) => SystemCommands.RestoreWindow(this);

        private void CloseWindow_CanExecute(object sender, CanExecuteRoutedEventArgs e) => e.CanExecute = true;
        private void CloseWindow_Executed(object sender, ExecutedRoutedEventArgs e) => SystemCommands.CloseWindow(this);
        #endregion

        #region MainImageEvents
        private void MainImage_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            Image image = sender as Image;
            if (sender.GetType().Name != "Image" || 
                e.Property.Name != "Source" || 
                image.GetAnimationBaseValue(Image.SourceProperty) == null)
                return;

            ImageController.ResetTransformations();
            FitImageToController();
        }
        #endregion

        #region CursorChange
        private void InitializeCursors()
        {
            grabCursor = GetCursor("HandGrab");
            grabbingCursor = GetCursor("HandGrabbing");
            MainImage.Cursor = grabCursor;
        }

        private Cursor GetCursor(string cursorName)
        {
            var buffer = Properties.Resources.ResourceManager.GetObject(cursorName) as byte[];
            
            using (var memoryStream = new MemoryStream(buffer))
            {
                return new Cursor(memoryStream);
            }
        }

        private void ImageController_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed || 
                Mouse.RightButton == MouseButtonState.Pressed)
                MainImage.Cursor = grabbingCursor;
        }

        private void ImageController_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Released || 
                Mouse.RightButton == MouseButtonState.Released)
                MainImage.Cursor = grabCursor;
        }
        #endregion

        #region ToggleFullscreen
        private void ToggleFullscreenButton_Checked(object sender, RoutedEventArgs e)
        {
            WindowStyle = WindowStyle.None;
            ResizeMode = ResizeMode.NoResize;
            if (WindowState == WindowState.Maximized)
                WindowState = WindowState.Normal;
            WindowState = WindowState.Maximized;
        }

        private void ToggleFullscreenButton_Unchecked(object sender, RoutedEventArgs e)
        {
            WindowStyle = WindowStyle.SingleBorderWindow;
            ResizeMode = ResizeMode.CanResize;
            WindowState = WindowState.Normal;
        }
        #endregion

        #region ResetTransformation
        private void ResetTransformationsButton_Click(object sender, RoutedEventArgs e)
        {
            ImageController.ResetTransformations();
            FitImageToController();
        }
        #endregion

        #region FitImageToController
        private void ImageController_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (MainImage.Source == null)
                return;

            FitImageToController();
        }

        private void FitImageToController()
        {
            if (MainImage.Source == null)
                return;

            BitmapImage image = (BitmapImage)MainImage.GetAnimationBaseValue(Image.SourceProperty);

            double controllerHeight = ImageController.ActualHeight;
            double controllerWidth = ImageController.ActualWidth;
            double imageHeight = image.PixelHeight;
            double imageWidth = image.PixelWidth;

            double scaleHeightRatio = double.NaN;
            double scaleWidthRatio = double.NaN;
            
            if (imageHeight > controllerHeight)
                scaleHeightRatio = controllerHeight / imageHeight;
            
            if (imageWidth > controllerWidth)
                scaleWidthRatio = controllerWidth / imageWidth;

            if (!double.IsNaN(scaleHeightRatio) && !double.IsNaN(scaleWidthRatio))
                ImageController.Scale = (scaleHeightRatio < scaleWidthRatio) ? scaleHeightRatio : scaleWidthRatio;
            else if (!double.IsNaN(scaleHeightRatio))
                ImageController.Scale = scaleHeightRatio;
            else if (!double.IsNaN(scaleWidthRatio))
                ImageController.Scale = scaleWidthRatio;
        }
        #endregion

        #region WindowSettings
        private void Window_Closed(object sender, EventArgs e)
        {
            SaveDefaultSettings();
        }

        private void SaveDefaultSettings()
        {
            Properties.Settings.Default.Top = Top;
            Properties.Settings.Default.Left = Left;
            Properties.Settings.Default.Width = Width;
            Properties.Settings.Default.Height = Height;

            if (WindowState == WindowState.Minimized)
                Properties.Settings.Default.WindowState = WindowState.Normal;
            else
                Properties.Settings.Default.WindowState = WindowState;

            Properties.Settings.Default.Save();
        }

        private void SetDefaultSettings()
        {
            double top = Properties.Settings.Default.Top;
            double left = Properties.Settings.Default.Left;
            double width = Properties.Settings.Default.Width;
            double height = Properties.Settings.Default.Height;
            WindowState windowState = Properties.Settings.Default.WindowState;

            if (Double.IsInfinity(top) || Double.IsNaN(top) ||
                Double.IsInfinity(left) || Double.IsNaN(left) ||
                Double.IsInfinity(width) || Double.IsNaN(width) ||
                Double.IsInfinity(height) || Double.IsNaN(height) ||
                !Enum.IsDefined(typeof(WindowState), windowState))
                Properties.Settings.Default.Reset();

            top = Properties.Settings.Default.Top;
            left = Properties.Settings.Default.Left;
            width = Properties.Settings.Default.Width;
            height = Properties.Settings.Default.Height;
            windowState = Properties.Settings.Default.WindowState;
        
            double screenHeight = SystemParameters.PrimaryScreenHeight;
            double screenWidth = SystemParameters.PrimaryScreenWidth;

            WindowState = windowState;
            Height = height;
            Width = width;
            Left = (left == -9999) ? (screenWidth / 2) - (Width / 2) : left;
            Top = (top == -9999) ? (screenHeight / 2) - (Height / 2) : top;
        }
        #endregion

        #region DragEvents
        private void MainWindow_DragEnter(object sender, DragEventArgs e)
        {
            if (!isDragAdornerAdded)
            {
                AdornerLayer.GetAdornerLayer(ImageController).Add(dragAdorner);
                isDragAdornerAdded = true;
            }
        }

        private void MainWindow_DragLeave(object sender, DragEventArgs e)
        {
            if (isDragAdornerAdded)
            {
                AdornerLayer.GetAdornerLayer(ImageController).Remove(dragAdorner);
                isDragAdornerAdded = false;
            }
        }

        private void MainWindow_PreviewDrop(object sender, DragEventArgs e)
        {
            if (isDragAdornerAdded)
            {
                AdornerLayer.GetAdornerLayer(ImageController).Remove(dragAdorner);
                isDragAdornerAdded = false;
            }
        }
        #endregion
    }
}