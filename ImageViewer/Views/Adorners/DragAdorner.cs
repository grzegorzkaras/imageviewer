﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Effects;

namespace ImageViewer.Views.Adorners
{
    public class DragAdorner : Adorner
    {
        public int CornerLineMargin { get; set; } = 20;
        public int CornerLineLength { get; set; } = 50;
        public int CornerLineWidth { get; set; } = 25;
        public Brush AdornerColor { get; set; } = new SolidColorBrush(Color.FromArgb(225, 255, 255, 255));

        public DragAdorner(UIElement adornedElement) : base(adornedElement)
        {
            IsHitTestVisible = false;
        }

        protected override void OnRender(DrawingContext dc)
        {
            if (RenderSize.Height <= CornerLineLength * 2)
                return;

            Point topLeftCornerStart =     new Point(CornerLineMargin, CornerLineMargin);
            Point bottomLeftCornerStart =  new Point(CornerLineMargin, RenderSize.Height - CornerLineMargin);
            Point topRightCornerStart =    new Point(RenderSize.Width - CornerLineMargin, CornerLineMargin);
            Point bottomRightCornerStart = new Point(RenderSize.Width - CornerLineMargin, RenderSize.Height - CornerLineMargin);

            RectangleGeometry topLeftRect =     new RectangleGeometry(new Rect(topLeftCornerStart,     new Point(topLeftCornerStart.X     + CornerLineLength, topLeftCornerStart.Y     + CornerLineLength)));
            RectangleGeometry bottomLeftRect =  new RectangleGeometry(new Rect(bottomLeftCornerStart,  new Point(bottomLeftCornerStart.X  + CornerLineLength, bottomLeftCornerStart.Y  - CornerLineLength)));
            RectangleGeometry topRightRect =    new RectangleGeometry(new Rect(topRightCornerStart,    new Point(topRightCornerStart.X    - CornerLineLength, topRightCornerStart.Y    + CornerLineLength)));
            RectangleGeometry bottomRightRect = new RectangleGeometry(new Rect(bottomRightCornerStart, new Point(bottomRightCornerStart.X - CornerLineLength, bottomRightCornerStart.Y - CornerLineLength)));

            PathGeometry LeftRects =  Geometry.Combine(topLeftRect,  bottomLeftRect,  GeometryCombineMode.Union, null);
            PathGeometry rightRects = Geometry.Combine(topRightRect, bottomRightRect, GeometryCombineMode.Union, null);
            PathGeometry allRects =   Geometry.Combine(LeftRects,    rightRects,      GeometryCombineMode.Union, null);

            CombinedGeometry corners = new CombinedGeometry(GeometryCombineMode.Exclude, allRects, 
                new RectangleGeometry(
                    new Rect(
                        new Point(topLeftCornerStart.X     + CornerLineWidth, topLeftCornerStart.Y     + CornerLineWidth), 
                        new Point(bottomRightCornerStart.X - CornerLineWidth, bottomRightCornerStart.Y - CornerLineWidth)
                    )
                ));

            Point centerPoint = new Point(RenderSize.Width / 2, RenderSize.Height / 2);
            CombinedGeometry plusSign = new CombinedGeometry(GeometryCombineMode.Union, 
                new RectangleGeometry(new Rect(new Point(centerPoint.X - (CornerLineWidth / 2), centerPoint.Y - (CornerLineLength / 1.5)), new Point(centerPoint.X + (CornerLineWidth / 2), centerPoint.Y + (CornerLineLength / 1.5)))), 
                new RectangleGeometry(new Rect(new Point(centerPoint.X - (CornerLineLength / 1.5), centerPoint.Y - (CornerLineWidth / 2)), new Point(centerPoint.X + (CornerLineLength / 1.5), centerPoint.Y + (CornerLineWidth / 2)))));
            
            dc.DrawGeometry(AdornerColor, null, corners);
            dc.DrawGeometry(AdornerColor, null, plusSign);
        }
    }
}
