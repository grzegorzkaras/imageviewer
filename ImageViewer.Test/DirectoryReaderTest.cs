﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ImageViewer.Services;
using System.IO;

namespace ImageViewer.Test
{
    [TestFixture]
    class DirectoryReaderTest
    {
        DirectoryContentReader directoryReader;

        string testDirectoryPath = @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\";
        string[] testDirectoryContents = {
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\broken.jpg",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\empty.bmp",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\non-animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\not-empty.bmp",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\text-file.txt",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\working.jpg"
        };

        string testSubDirectoryPath = @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\SubDirectory1\";
        string[] testSubDirectoryContents = {
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\SubDirectory1\animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\SubDirectory1\broken.jpg",
        };

        string[] correctImagesPaths = {
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\non-animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\not-empty.bmp",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\working.jpg"
        };

        public DirectoryReaderTest()
        {
            directoryReader = new DirectoryContentReader(new ImageFilesFilter());
        }

        [Test]
        public void DefaultDirectoryIsNull()
        {
            DirectoryContentReader clearDirectoryReader = new DirectoryContentReader(new ImageFilesFilter());
            string[] paths = clearDirectoryReader.FilesPaths;
            Assert.IsNull(paths);
        }

        [Test]
        public void ReadTestDirectory()
        {
            string[] paths = directoryReader.GetFiles(testDirectoryPath);
            CollectionAssert.AreEquivalent(testDirectoryContents, paths);

            string[] currentDirectory = directoryReader.FilesPaths;
            CollectionAssert.AreEquivalent(testDirectoryContents, currentDirectory);

            string[] paths2 = directoryReader.GetFiles(testSubDirectoryPath);
            CollectionAssert.AreEquivalent(testSubDirectoryContents, paths2);

            string[] currentDirectory2 = directoryReader.FilesPaths;
            CollectionAssert.AreEquivalent(testSubDirectoryContents, currentDirectory2);
        }

        [Test]
        public void CheckFilesCount()
        {
            DirectoryContentReader clearDirectoryReader = new DirectoryContentReader(new ImageFilesFilter());
            Assert.AreEqual(0, clearDirectoryReader.FilesCount);

            clearDirectoryReader.GetFiles(testDirectoryPath);
            Assert.AreEqual(testDirectoryContents.Length, clearDirectoryReader.FilesCount);
        }

        [Test]
        public void CheckImageFilesCount()
        {
            DirectoryContentReader clearDirectoryReader = new DirectoryContentReader(new ImageFilesFilter());
            Assert.AreEqual(0, clearDirectoryReader.FilesCount);

            clearDirectoryReader.GetFiles(testDirectoryPath);
            Assert.AreEqual(correctImagesPaths.Length, clearDirectoryReader.FilesCount);
        }

        [Test]
        public void CheckImageFilter()
        {
            DirectoryContentReader clearDirectoryReader = new DirectoryContentReader(new ImageFilesFilter());
            clearDirectoryReader.GetFiles(testDirectoryPath);

            string[] paths = directoryReader.GetFiles(testDirectoryPath);

            string[] imagesPaths = directoryReader.GetFiles(paths);
            CollectionAssert.AreEquivalent(correctImagesPaths, imagesPaths);

            string[] imagesPaths2 = directoryReader.GetFiles(testDirectoryPath);
            CollectionAssert.AreEquivalent(correctImagesPaths, imagesPaths2);
        }

        [Test]
        public void ChectImageFilterDI()
        {
            DirectoryContentReader clearDirectoryReader = new DirectoryContentReader(new ImageFilesFilter());
            string[] paths = clearDirectoryReader.GetFiles(testDirectoryPath);

            Assert.Throws<NullReferenceException>(() => clearDirectoryReader.GetFiles(paths));
        }
    }
}
