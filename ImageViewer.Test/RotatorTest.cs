﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ImageViewer.Services;

namespace ImageViewer.Test
{
    [TestFixture]
    public class RotatorTest
    {

        private int[] testArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        [Test]
        public void InitWithStartIndex()
        {
            Rotator<int> rotator = new Rotator<int>(testArray, 5);
            Assert.AreEqual(testArray[5], rotator.Current());
        }

        [Test]
        public void InitWithoutStartIndex()
        {
            Rotator<int> rotator = new Rotator<int>(testArray);
            Assert.AreEqual(testArray[0], rotator.Current());
        }

        [Test]
        public void Rotation()
        {
            Rotator<int> rotator = new Rotator<int>(testArray, 5);
            Assert.AreEqual(testArray[5], rotator.Current());
            rotator.Next();
            rotator.Next();
            rotator.Next();
            rotator.Next();
            Assert.AreEqual(testArray[9], rotator.Current());
            rotator.Next();
            Assert.AreEqual(testArray[0], rotator.Current());
            rotator.Next();
            Assert.AreEqual(testArray[1], rotator.Current());
            rotator.Previous();
            Assert.AreEqual(testArray[0], rotator.Current());
            rotator.Previous();
            Assert.AreEqual(testArray[9], rotator.Current());
            rotator.Previous();
            rotator.Previous();
            rotator.Previous();
            rotator.Previous();
            Assert.AreEqual(testArray[5], rotator.Current());
            Assert.AreEqual(testArray[4], rotator.Previous());
            Assert.AreEqual(testArray[5], rotator.Next());
        }

        [Test]
        public void NextEdge()
        {
            Rotator<int> rotator = new Rotator<int>(testArray, 9);
            Assert.AreEqual(testArray[0], rotator.Next());
        }

        [Test]
        public void PreviousEdge()
        {
            Rotator<int> rotator = new Rotator<int>(testArray, 0);
            Assert.AreEqual(testArray[9], rotator.Previous());
        }
    }
}
