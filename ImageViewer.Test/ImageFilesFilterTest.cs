﻿using ImageViewer.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer.Test
{
    [TestFixture]
    class ImageFilesFilterTest
    {
        private IFilesFilter imageFilter;

        string[] testDirectoryContents = {
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\broken.jpg",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\empty.bmp",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\non-animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\not-empty.bmp",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\text-file.txt",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\working.jpg"
        };
        string[] correctImagesPaths = {
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\non-animated.gif",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\not-empty.bmp",
            @"G:\C#\ImageViewer\ImageViewer.Test\TestDirectory\working.jpg"
        };

        public ImageFilesFilterTest()
        {
            imageFilter = new ImageFilesFilter();
        }
        
        [Test]
        public void FilterAllTest()
        {
            string[] imagesPaths = imageFilter.FilterAll(testDirectoryContents);
            CollectionAssert.AreEquivalent(correctImagesPaths, imagesPaths);
        }

    }
}
