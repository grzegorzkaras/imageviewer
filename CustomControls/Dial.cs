﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DialControl
{
    public class Dial : Control
    {
        public delegate void AngleChangedEventHandler(object sender, RoutedPropertyChangedEventArgs<double> e);
        public event AngleChangedEventHandler AngleChanged;
        protected virtual void OnAngleChanged(RoutedPropertyChangedEventArgs<double> e) 
            => AngleChanged?.Invoke(this, e);

        static Dial()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(Dial), 
                new FrameworkPropertyMetadata(typeof(Dial)));
        }

        private bool isMouseRotating = false;
        private Point arrowCenterPoint;
        private Vector mouseDownVector;
        private double mouseDownAngle;

        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register(
                "Angle", 
                typeof(double), 
                typeof(Dial), 
                new FrameworkPropertyMetadata(
                    0.0, 
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, 
                    null, 
                    CoerceValueCallback
                )
            );

        public double Angle
        {
            get => (double)GetValue(AngleProperty);
            set
            {
                double oldAngle = Angle;
                SetValue(AngleProperty, value);
                OnAngleChanged(new RoutedPropertyChangedEventArgs<double>(oldAngle, Angle));
            }
        }

        static object CoerceValueCallback(DependencyObject d, object baseValue)
        {
            var angle = (double)baseValue % 360;
            return (angle < 0) ? angle + 360 : angle;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            var mouseDownPoint = e.GetPosition(this);
            mouseDownVector = mouseDownPoint - arrowCenterPoint;
            mouseDownAngle = Angle;
            e.MouseDevice.Capture(this);
            isMouseRotating = true;
            base.OnMouseDown(e);
        }

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            arrowCenterPoint = new Point(ActualWidth / 2, ActualHeight / 2);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (isMouseRotating)
            {
                Point currentPosition = e.GetPosition(this);
                Vector currentVector = currentPosition - arrowCenterPoint;
                Angle = Vector.AngleBetween(mouseDownVector, currentVector) + mouseDownAngle;
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (isMouseRotating)
            {
                e.MouseDevice.Capture(null);
                isMouseRotating = false;
            }
            base.OnMouseUp(e);
        }

        protected override void OnLostMouseCapture(MouseEventArgs e)
        {
            isMouseRotating = false;
            base.OnLostMouseCapture(e);
        }
    }
}
